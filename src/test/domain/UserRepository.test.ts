import User from "../../main/domain/User";
import InMemoryUserRepository from "../../main/infrastructure/services/InMemoryUserRepository";

describe("Use Repository Test", () => {

    test("should save an user on add method", () => {
        const userRepository = getUserRepository();
        const user = new User("Pepe", "pepe");

        userRepository.add(user);
        const recoveredUser = userRepository.getByNickname("pepe")

        expect(recoveredUser).toBe(user);
    });

    test("should return an user from existing nickname", () => {
        const userRepository = getUserRepository();
        const user = new User("Pepe", "pepe");
        userRepository.add(user);

        const recoveredUser = userRepository.getByNickname("pepe")

        expect(recoveredUser).not.toBeUndefined();
        expect(recoveredUser).not.toBeNull();
    });

    test("should return undefined on a unregistered nickname request", () => {
        const userRepository = getUserRepository();
        const recoveredUser = userRepository.getByNickname("pepe");
        expect(recoveredUser).toBeUndefined();
    })

});

const getUserRepository = () => {
    return new InMemoryUserRepository();
}