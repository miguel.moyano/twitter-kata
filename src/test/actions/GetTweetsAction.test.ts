import GetTweetsAction from "../../main/actions/GetTweetsAction";
import User from "../../main/domain/User";
import InMemoryTweetRepository from "../../main/infrastructure/services/InMemoryTweetRepository";
import InMemoryUserRepository from "../../main/infrastructure/services/InMemoryUserRepository";

describe("Get tweets action", () => {
    test("get an empty array from existing username without tweets", () => {
        const userRepository = getUserRepository();
        const tweetRepository = getTweetRepository();
        userRepository.add(new User("Migue", "elmiguedev"));

        const action = new GetTweetsAction(userRepository, tweetRepository);

        const tweets = action.execute({
            nickname: "elmiguedev"
        });

        expect(Array.isArray(tweets)).toBe(true);
    })

    test("get array with one tweet after user tweets at first time", () => {
        const userRepository = getUserRepository();
        const tweetRepository = getTweetRepository();
        userRepository.add(new User("Migue", "elmiguedev"));
        tweetRepository.add({ nickname: "elmiguedev", message: "Hola mundo!" });

        const action = new GetTweetsAction(userRepository, tweetRepository);

        const tweets = action.execute({
            nickname: "elmiguedev"
        });

        expect(Array.isArray(tweets)).toBe(true);
        expect(tweets.length).toBe(1);
    })

    test("get an exception when user does not exists", () => {
        const userRepository = getUserRepository();
        const tweetRepository = getTweetRepository();

        const action = new GetTweetsAction(userRepository, tweetRepository);

        expect(() => {
            const tweets = action.execute({
                nickname: "elmiguedev"
            });
        }).toThrowError("User does not exists");
    })
})

const getUserRepository = () => {
    return new InMemoryUserRepository();
}

const getTweetRepository = () => {
    return new InMemoryTweetRepository();
}