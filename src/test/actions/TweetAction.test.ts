import TweetAction from "../../main/actions/TweetAction";
import User from "../../main/domain/User";
import InMemoryTweetRepository from "../../main/infrastructure/services/InMemoryTweetRepository";
import InMemoryUserRepository from "../../main/infrastructure/services/InMemoryUserRepository";

describe("Twitter action", () => {

    test("should add add a tweet", () => {
        const userRepository = getUserRepository();
        const tweetRepository = getTweetRepository();
        userRepository.add(new User("Migue", "elmiguedev"));
        const action = new TweetAction(userRepository, tweetRepository);
        const nickname = "elmiguedev";
        const message = "Hola, este es un twit";

        const tweet = action.execute({nickname, message});

        expect(tweet).not.toBeNull();
        expect(tweet).not.toBeUndefined();
    });

    test("should throw an exception if nickname does not exists", () => {
        const userRepository = getUserRepository();
        const tweetRepository = getTweetRepository();
        const action = new TweetAction(userRepository, tweetRepository);
        const nickname = "pepe";
        const message = "Hola, este es un twit";

        expect(() => {
            const tweet = action.execute({nickname, message});
        }).toThrowError("User does not exists");
    })

})

const getUserRepository = () => {
    return new InMemoryUserRepository();
}

const getTweetRepository = () => {
    return new InMemoryTweetRepository();
}