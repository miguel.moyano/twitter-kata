import UpdateUserNameAction from "../../main/actions/UpdateUserNameAction";
import User from "../../main/domain/User";
import InMemoryUserRepository from "../../main/infrastructure/services/InMemoryUserRepository";

describe("Update user action ", () => {

    test("should exists", () => {
        const action = new UpdateUserNameAction(getUserRepository());
        expect(action).not.toBeUndefined();
    });

    test("new name should be saved after change it", () => {
        const userRepository = getUserRepository();
        const action = new UpdateUserNameAction(userRepository);
        const nickname = "pepe";
        const name = "Pepe's New Name";

        action.execute({
            nickname,
            name
        });

        const recoveredUser = userRepository.getByNickname(nickname);
        expect(recoveredUser.getName()).toBe(name);
    });

    test("should throw an error if nickname is not registered", () => {
        const userRepository = getUserRepository();
        const action = new UpdateUserNameAction(userRepository);
        const nickname = "anothernick";
        const name = "anothername";

        expect(() => {
            action.execute({
                nickname,
                name
            });
        }).toThrowError("User does not exists");
    })

})

const getUserRepository = () => {
    const repository = new InMemoryUserRepository();
    repository.add(new User("Pepe Argento", "pepe"));
    repository.add(new User("Dardo Fuseneco", "dardo"));
    return repository;
}