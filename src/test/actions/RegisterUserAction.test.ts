import RegisterUserAction from "../../main/actions/RegisterUserAction";
import User from "../../main/domain/User";
import InMemoryUserRepository from "../../main/infrastructure/services/InMemoryUserRepository";

describe("Register user action", () => {

    test("should create an action", () => {
        const userRepository = getUserRepositoryMock();
        const action = new RegisterUserAction(userRepository);
        expect(action).not.toBeNull();
    })

    test("should create an user from name and nickname", () => {
        const name = "Pepe Argento";
        const nickname = "pepe";
        const userRepository = getUserRepositoryMock();
        const action = new RegisterUserAction(userRepository);

        const createdUser: User = action.execute({
            name, nickname
        });

        expect(createdUser.getName()).toBe(name);
        expect(createdUser.getNickname()).toBe(nickname);
    });

    test("new user should not have the same nickname than another user", () => {
        const userRepository = getUserRepositoryMock();
        const action = new RegisterUserAction(userRepository);
        action.execute({
            name: "Pepe Argento",
            nickname: "pepe"
        });

        expect(() => {
            action.execute({
                name: "Jose Argento",
                nickname: "pepe"
            });
        }).toThrowError("User nickname alredy exists");
    });

})

const getUserRepositoryMock = () => {
    return new InMemoryUserRepository();
}

