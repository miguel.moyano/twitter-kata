import FollowUserAction from "../../main/actions/FollowUserAction";
import User from "../../main/domain/User";
import InMemoryUserRepository from "../../main/infrastructure/services/InMemoryUserRepository";

describe("Follow user action", () => {

    test("should create an action", () => {
        const userRepository = getUserRepository();
        const action = new FollowUserAction(userRepository);
        expect(action).not.toBeNull();
    });

    test("should perform a follow from an user to another", () => {
        const userRepository = getUserRepository();
        const action = new FollowUserAction(userRepository);

        expect(() => {
            action.execute({
                nickname: "pepe",
                nicknameToFollow: "moni"
            });
        }).not.toThrowError();
    });

    test("should throw an error if some nickname doesn't exists", () => {
        const userRepository = getUserRepository();
        const action = new FollowUserAction(userRepository);

        expect(() => {
            action.execute({
                nickname: "test",
                nicknameToFollow: "test2"
            });
        }).toThrowError();
    });

    test("should throw an error if an user is trying to follow itself", () => {
        const userRepository = getUserRepository();
        const action = new FollowUserAction(userRepository);

        expect(() => {
            action.execute({
                nickname: "pepe",
                nicknameToFollow: "pepe"
            })
        }).toThrowError("User cannot follow itself")
    })

    test("should throw an error if try to follow once again", () => {
        const userRepository = getUserRepository();
        const action = new FollowUserAction(userRepository);
        action.execute({
            nickname: "pepe",
            nicknameToFollow: "moni"
        })
        expect(() => {
            action.execute({
                nickname: "pepe",
                nicknameToFollow: "moni"
            })
        }).toThrowError("User alredy followed")
    })


});

const getUserRepository = () => {
    const repository = new InMemoryUserRepository();
    repository.add(new User("Pepe Argento", "pepe"));
    repository.add(new User("Moni Argento", "moni"));
    repository.add(new User("Dardo fuseneco", "dardo"));
    repository.add(new User("Maria Elena fuseneco", "mari"));
    return repository;
}