import GetUserFollowersAction from "../../main/actions/GetUserFollowersAction";
import User from "../../main/domain/User";
import InMemoryUserRepository from "../../main/infrastructure/services/InMemoryUserRepository";
import FollowUserAction from "../../main/actions/FollowUserAction";

describe("Get user followers action", () => {

    test("should action exists", () => {
        const userRepository = getUserRepository();
        const action = new GetUserFollowersAction(userRepository);
        expect(action).not.toBeUndefined();
    });

    test("should return and empty array if users has not followers", () => {
        const userRepository = getUserRepository();
        const action = new GetUserFollowersAction(userRepository);
        const nickname = "pepe";

        const users = action.execute({ nickname });

        expect(users.length).toBe(0);
    });

    test("should return one follower", () => {
        const userRepository = getUserRepository();
        const getFollowersAction = new GetUserFollowersAction(userRepository);
        const followAction = new FollowUserAction(userRepository);

        followAction.execute({
            nickname: "pepe",
            nicknameToFollow: "moni"
        });

        const users = getFollowersAction.execute({
            nickname: "moni"
        });

        expect(users.length).toBe(1);

    });

    test("should return three followers", () => {
        const userRepository = getUserRepository();
        const action = new GetUserFollowersAction(userRepository);
        const followAction = new FollowUserAction(userRepository);

        followAction.execute({ nickname: "moni", nicknameToFollow: "pepe" });
        followAction.execute({ nickname: "dardo", nicknameToFollow: "pepe" });
        followAction.execute({ nickname: "mari", nicknameToFollow: "pepe" });

        const users = action.execute({ nickname: "pepe" });

        expect(users.length).toBe(3);
    });

    test("should throw an error if nickname is not regitered", () => {
        const userRepository = getUserRepository();
        const action = new GetUserFollowersAction(userRepository);
        const nickname = "fatiga";

        expect(() => {
            const users = action.execute({ nickname });
        }).toThrowError("User does not exists");
    })
})

const getUserRepository = () => {
    const repository = new InMemoryUserRepository();
    repository.add(new User("Pepe Argento", "pepe"));
    repository.add(new User("Moni Argento", "moni"));
    repository.add(new User("Dardo fuseneco", "dardo"));
    repository.add(new User("Maria Elena fuseneco", "mari"));
    return repository;
}