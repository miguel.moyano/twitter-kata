import RegisterUserAction from "../../../../main/actions/RegisterUserAction"
import UserHandler from "../../../../main/infrastructure/api/handlers/UserHandler"
import InMemoryUserRepository from "../../../../main/infrastructure/services/InMemoryUserRepository"
import { getMockReq, getMockRes } from "@jest-mock/express"
import User from "../../../../main/domain/User"
import FollowUserAction from "../../../../main/actions/FollowUserAction"
import GetUserFollowersAction from "../../../../main/actions/GetUserFollowersAction"
import UpdateUserNameAction from "../../../../main/actions/UpdateUserNameAction"

describe("user api register handler", () => {

    test("should return 400 status if doesn't recibe name and nickname on request", () => {
        const registerAction = getRegisterAction();
        const userHander = new UserHandler(null, null, registerAction, null);

        const req = getMockReq()
        const res = getMockRes().res;

        userHander.register(req, res);

        expect(res.status).toBeCalledWith(400);
    });

    test("should return 201 and user on success registration", () => {
        const registerAction = getRegisterAction();
        const userHander = new UserHandler(null, null, registerAction, null);

        const req = getMockReq({
            body: {
                name: "migue",
                nickname: "elmiguedev"
            }
        });
        const res = getMockRes().res;
        const expectedUser = new User("migue", "elmiguedev");
        userHander.register(req, res);

        expect(res.status).toBeCalledWith(201);
        expect(res.json).toBeCalledWith(expectedUser);
    });

    test("should return 400 on a registration action exception", () => {
        const registerAction = getRegisterAction();
        const userHander = new UserHandler(null, null, registerAction, null);

        const req = getMockReq({
            body: {
                name: "migue",
                nickname: "elmiguedev"
            }
        });
        const res = getMockRes().res;

        userHander.register(req, res);
        userHander.register(req, res);

        expect(res.status).toBeCalledWith(400);
    })
})

describe("user api follow handler", () => {
    test("should return 400 status if doesn't recibe both nicknames", () => {
        const followAction = getFollowAction();
        const userHander = new UserHandler(followAction, null, null, null);

        const req = getMockReq();
        const res = getMockRes().res;

        userHander.follow(req, res);

        expect(res.status).toBeCalledWith(400);
    });

    test("should return 200 on success follow", () => {
        const followAction = getFollowAction();
        const userHander = new UserHandler(followAction, null, null, null);

        const req = getMockReq({
            body: {
                nickname: "elmiguedev",
                nicknameToFollow: "pepe"
            }
        });
        const res = getMockRes().res;

        userHander.follow(req, res);

        expect(res.status).toBeCalledWith(200);
    });

    test("should return 400 on a follow action exception", () => {
        const followAction = getFollowAction();
        const userHander = new UserHandler(followAction, null, null, null);

        const req = getMockReq({
            body: {
                nickname: "elmiguedev",
                nicknameToFollow: "elchavo"
            }
        });
        const res = getMockRes().res;

        userHander.register(req, res);
        userHander.register(req, res);

        expect(res.status).toBeCalledWith(400);
    })
});

describe("user api get followers handler", () => {
    test("get 400 on invalid request data", () => {
        const action = getGetFollowersAction();
        const handler = new UserHandler(null, action, null, null);
        const req = getMockReq();
        const res = getMockRes().res;
        handler.getFollowers(req, res);

        expect(res.status).toBeCalledWith(400);
    });

    test("get 200 and users array on existing user request", () => {
        const action = getGetFollowersAction();
        const handler = new UserHandler(null, action, null, null);
        const req = getMockReq({
            params: {
                nickname: "pepe"
            }
        });
        const res = getMockRes().res;
        const expectedResponse = ["elmiguedev"];

        handler.getFollowers(req, res);

        expect(res.status).toBeCalledWith(200);
        expect(res.json).toBeCalledWith(expectedResponse);
    })

    test("get 400 on a action exception", () => {
        const action = getGetFollowersAction();
        const handler = new UserHandler(null, action, null, null);
        const req = getMockReq({
            params: {
                nickname: "moni"
            }
        });
        const res = getMockRes().res;

        handler.getFollowers(req, res);

        expect(res.status).toBeCalledWith(400);
    })
});

describe("user api update name handler", () => {
    test("get 400 on invalid data request", () => {
        const action = getUpdateNameAction();
        const handler = new UserHandler(null, null, null, action);
        const req = getMockReq();
        const res = getMockRes().res;
        handler.updateName(req, res);
        expect(res.status).toBeCalledWith(400);
    });

    test("get 200 and updated data for a valid request", () => {
        const action = getUpdateNameAction();
        const handler = new UserHandler(null, null, null, action);
        const req = getMockReq({ body: { nickname: "pepe", name: "Pepe Argento" } });
        const res = getMockRes().res;
        const expectedResponse = new User("Pepe Argento", "pepe");
        handler.updateName(req, res);
        expect(res.status).toBeCalledWith(200);
        expect(res.json).toBeCalledWith(expectedResponse);
    });

    test("get 400 on action exception", () => {
        const action = getUpdateNameAction();
        const handler = new UserHandler(null, null, null, action);
        const req = getMockReq({ body: { nickname: "moni", name: "Moni Argento" } });
        const res = getMockRes().res;
        handler.updateName(req, res);
        expect(res.status).toBeCalledWith(400);
    })
});

const getRegisterAction = () => {
    const userRepository = new InMemoryUserRepository();
    const registerAction = new RegisterUserAction(userRepository);
    return registerAction;
}

const getFollowAction = () => {
    const userRepository = new InMemoryUserRepository();
    userRepository.add(new User("Migue", "elmiguedev"));
    userRepository.add(new User("Pepe Argento", "pepe"));
    const followAction = new FollowUserAction(userRepository);
    return followAction;
}

const getGetFollowersAction = () => {
    const userRepository = new InMemoryUserRepository();
    const migue = new User("Migue", "elmiguedev");
    const pepe = new User("Pepe", "pepe");
    migue.follow(pepe.getNickname());
    userRepository.add(pepe);
    userRepository.add(migue);
    const getFollowersAction = new GetUserFollowersAction(userRepository);
    return getFollowersAction;
}

const getUpdateNameAction = () => {
    const userRepository = new InMemoryUserRepository();
    userRepository.add(new User("pepe", "pepe"));
    const action = new UpdateUserNameAction(userRepository);
    return action;
}