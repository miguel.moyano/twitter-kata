import { getMockReq, getMockRes } from "@jest-mock/express";
import GetTweetsAction from "../../../../main/actions/GetTweetsAction";
import TweetAction from "../../../../main/actions/TweetAction";
import User from "../../../../main/domain/User";
import TweetHandler from "../../../../main/infrastructure/api/handlers/TweetHandler";
import InMemoryTweetRepository from "../../../../main/infrastructure/services/InMemoryTweetRepository";
import InMemoryUserRepository from "../../../../main/infrastructure/services/InMemoryUserRepository";

describe("new tweet handler", () => {

    test("should return 400 status if doesn't recibe nickname and message on request", () => {
        const tweetAction = getTweetAction();
        const tweetHander = new TweetHandler(tweetAction, null);

        const req = getMockReq({ body: {} })
        const res = getMockRes().res;

        tweetHander.addTweet(req, res);

        expect(res.status).toBeCalledWith(400);
    });

    test("should return 201 and user on success registration", () => {
        const tweetAction = getTweetAction();
        const tweetHander = new TweetHandler(tweetAction, null);
        const expectedResponse = {
            nickname: "elmiguedev",
            message: "Hola mundo!"
        };

        const req = getMockReq({
            body: {
                nickname: "elmiguedev",
                message: "Hola mundo!"
            }
        })
        const res = getMockRes().res;

        tweetHander.addTweet(req, res);

        expect(res.status).toBeCalledWith(201);
        expect(res.json).toBeCalledWith(expectedResponse);
    });

    test("should return 400 on a registration action exception", () => {
        const tweetAction = getTweetAction();
        const tweetHander = new TweetHandler(tweetAction, null);

        const req = getMockReq({
            body: {
                nickname: "pepe",
                message: "hola"
            }
        })
        const res = getMockRes().res;

        tweetHander.addTweet(req, res);

        expect(res.status).toBeCalledWith(400);
    })


});

describe("get tweets handler", () => {

    test("should return 400 status if doesn't recibe nickname on request", () => {
        const getTweetsAction = getGetTweetsAction();
        const tweetHander = new TweetHandler(null, getTweetsAction);

        const req = getMockReq()
        const res = getMockRes().res;

        tweetHander.getTweets(req, res);

        expect(res.status).toBeCalledWith(400);
    });

    test("should return 200 and tweets array on success", () => {
        const getTweetsAction = getGetTweetsAction();
        const tweetHander = new TweetHandler(null, getTweetsAction);

        const expectedResponse = [
            { nickname: "elmiguedev", message: "hola mundo" }
        ];

        const req = getMockReq({
            params: {
                nickname: "elmiguedev"
            }
        })
        const res = getMockRes().res;

        tweetHander.getTweets(req, res);

        expect(res.status).toBeCalledWith(200);
        expect(res.json).toBeCalledWith(expectedResponse);
    });

    test("should return 400 on a action exception", () => {
        const getTweetsAction = getGetTweetsAction();
        const tweetHander = new TweetHandler(null, getTweetsAction);

        const req = getMockReq({
            params: {
                nickname: "pepe"
            }
        })
        const res = getMockRes().res;

        tweetHander.getTweets(req, res);

        expect(res.status).toBeCalledWith(400);
    })


});


const getTweetAction = () => {
    const userRepository = new InMemoryUserRepository();
    userRepository.add(new User("Migue", "elmiguedev"));

    const tweetRepository = new InMemoryTweetRepository();
    return new TweetAction(userRepository, tweetRepository);
}

const getGetTweetsAction = () => {
    const userRepository = new InMemoryUserRepository();
    userRepository.add(new User("Migue", "elmiguedev"));

    const tweetRepository = new InMemoryTweetRepository();
    tweetRepository.add({ nickname: "elmiguedev", message: "hola mundo" });

    return new GetTweetsAction(userRepository, tweetRepository);
}