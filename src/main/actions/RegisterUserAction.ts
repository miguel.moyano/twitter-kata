import User from "../domain/User";
import UserRepository from "../domain/UserRepository";

export default class RegisterUserAction {

    private userRepository: UserRepository;

    constructor(userRepository: UserRepository) {
        this.userRepository = userRepository;
    }

    public execute(actionData: ActionData): User {
        if (this.userRepository.getByNickname(actionData.nickname)) {
            throw new Error("User nickname alredy exists");
        }
        const user = new User(actionData.name, actionData.nickname);
        this.userRepository.add(user);

        return user;
    }
}

interface ActionData {
    name: string;
    nickname: string;
}

