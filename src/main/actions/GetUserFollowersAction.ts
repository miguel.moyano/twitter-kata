import UserRepository from "../domain/UserRepository";

export default class GetUserFollowersAction {

    private userRepository: UserRepository;

    constructor(userRepository: UserRepository) {
        this.userRepository = userRepository;
    }

    public execute(actionData: ActionData): Array<string> {
        if (!this.userRepository.getByNickname(actionData.nickname)) {
            throw new Error("User does not exists");
        }

        return this.userRepository.getFollowersByNickname(actionData.nickname);
    }
}

interface ActionData {
    nickname: string;
}