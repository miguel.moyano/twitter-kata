
import User from "../domain/User";
import UserRepository from "../domain/UserRepository";

export default class UpdateUserNameAction {

    private userRepository: UserRepository;

    constructor(userRepository: UserRepository) {
        this.userRepository = userRepository;
    }

    execute(actionData: ActionData): User {
        const user = this.userRepository.getByNickname(actionData.nickname);
        if (!user) {
            throw new Error("User does not exists");
        }
        user.setName(actionData.name);
        this.userRepository.update(user);

        return user;
    }

}

interface ActionData {
    nickname: string;
    name: string;
}