import Tweet from "../domain/Tweet";
import TweetRepository from "../domain/TweetRepository";
import UserRepository from "../domain/UserRepository";

export default class GetTweetsAction {
    private userRepository: UserRepository;
    private tweetRepository: TweetRepository;

    constructor(userRepository: UserRepository, tweetRepository: TweetRepository) {
        this.userRepository = userRepository;
        this.tweetRepository = tweetRepository;
    }

    execute(actionData:ActionData) {
        if (!this.userRepository.getByNickname(actionData.nickname)) {
            throw new Error("User does not exists");
        }
        const tweets = this.tweetRepository.getTweets(actionData.nickname);
        return tweets;
    }
}

interface ActionData {
    nickname: string;
}