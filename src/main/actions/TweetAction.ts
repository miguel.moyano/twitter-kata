import Tweet from "../domain/Tweet";
import TweetRepository from "../domain/TweetRepository";
import UserRepository from "../domain/UserRepository";

export default class TweetAction {
    private userRepository: UserRepository;
    private tweetRepository: TweetRepository;

    constructor(userRepository: UserRepository, tweetRepository: TweetRepository) {
        this.userRepository = userRepository;
        this.tweetRepository = tweetRepository;
    }

    execute(actionData:ActionData) {
        if (!this.userRepository.getByNickname(actionData.nickname)) {
            throw new Error("User does not exists");
        }

        const tweet = {
            nickname: actionData.nickname,
            message: actionData.message
        }

        this.tweetRepository.add(tweet);

        return tweet;
    }
}

interface ActionData {
    nickname: string;
    message: string;
}