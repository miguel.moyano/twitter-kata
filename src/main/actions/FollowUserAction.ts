import User from "../domain/User";
import UserRepository from "../domain/UserRepository";

export default class FollowUserAction {
    private userRepository: UserRepository;

    constructor(userRepository: UserRepository) {
        this.userRepository = userRepository;
    }

    public execute(actionData: ActionData) {
        const userFollower = this.userRepository.getByNickname(actionData.nickname);
        const userToFollow = this.userRepository.getByNickname(actionData.nicknameToFollow);

        this.validateUsers(userFollower, userToFollow);

        userFollower.follow(actionData.nicknameToFollow);
        this.userRepository.update(userFollower);
    }

    private validateUsers(user: User, userToFollow: User) {
        if (!user || !userToFollow) {
            throw new Error("User does not exists");
        }

        if (user === userToFollow) {
            throw new Error("User cannot follow itself");
        }

        if (user.getFollowed().includes(userToFollow.getNickname())) {
            throw new Error("User alredy followed");
        }
    }

}

interface ActionData {
    nickname: string;
    nicknameToFollow: string;
}