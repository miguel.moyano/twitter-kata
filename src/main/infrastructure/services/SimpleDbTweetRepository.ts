import path from "path";
import JSONdb from "simple-json-db";
import Tweet from "../../domain/Tweet";
import TweetRepository from "../../domain/TweetRepository"
export default class SimpleDbTweetRepository implements TweetRepository {

    private db: JSONdb;

    constructor() {
        // this.users = new Map();
        const dbPath = path.join(__dirname, "../db/db.json");
        this.db = new JSONdb(dbPath);
        if (!this.db.has("tweets")) {
            this.db.set("tweets", []);
            this.db.sync();
        }
    }

    getTweets(nickname: string): Tweet[] {
        const tweets = <Array<Tweet>>this.db.get("tweets");
        return tweets.filter(t => t.nickname === nickname);
    }

    add(tweet: Tweet): void {
        const tweets = <Array<Tweet>>this.db.get("tweets");
        tweets.push(tweet);
        this.db.set("tweets", tweets);
    }

}