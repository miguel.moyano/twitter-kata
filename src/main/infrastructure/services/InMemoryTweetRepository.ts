import Tweet from "../../domain/Tweet";
import TweetRepository from "../../domain/TweetRepository";

export default class InMemoryTweetRepository implements TweetRepository {
    private tweets: Array<Tweet>;

    constructor() {
        this.tweets = new Array();
    }

    add(tweet: Tweet): void {
        this.tweets.push(tweet);
    }

    getTweets(nickname: string): Tweet[] {
        return this.tweets.filter(t => t.nickname === nickname);
    }

}