import User from "../../domain/User";
import UserRepository from "../../domain/UserRepository";

export default class InMemoryUserRepository implements UserRepository {
    private users: Map<string, User>;

    constructor() {
        this.users = new Map();
    }

    add(user: User): void {
        this.users.set(user.getNickname(), user);
    }

    update(user: User): void {
        const oldUser = this.users.get(user.getNickname());
        if (oldUser) {
            this.users.set(user.getNickname(), user);
        }
    }

    getByNickname(nickname: string): User {
        return this.users.get(nickname);
    }

    getFollowersByNickname(nickname: string): Array<string> {
        const followers = new Array<string>();
        this.users.forEach((user) => {
            if (user.getFollowed().includes(nickname)) {
                followers.push(user.getNickname());
            }
        });
        return followers;
    }

}