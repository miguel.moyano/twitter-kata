import User from "../../domain/User";
import UserRepository from "../../domain/UserRepository";
import JSONdb from "simple-json-db";
import path from "path";

export default class SimpleDbUserRepository implements UserRepository {
    // private users: Map<string, User>;
    private db: JSONdb;

    constructor() {
        // this.users = new Map();
        const dbPath = path.join(__dirname, "../db/db.json");
        this.db = new JSONdb(dbPath);
        if (!this.db.has("users")) {
            this.db.set("users", {});
            this.db.sync();
        }
    }

    add(user: User): void {
        const users = this.db.get("users");
        users[user.getNickname()] = user;
        this.db.set("users", users);
    }

    update(user: User): void {
        const users = this.db.get("users");
        const oldUser = users[user.getNickname()];
        if (oldUser) {
            users[user.getNickname()] = user;
        }
        this.db.set("users", users);
    }

    getByNickname(nickname: string): User {
        const users = this.db.get("users");
        const dbUser = users[nickname];
        if (dbUser) {
            const user = new User(dbUser.name, dbUser.nickname);
            user.setFollowed(dbUser.followedUsers);
            return user;
        }
        return undefined;
    }

    getFollowersByNickname(nickname: string): Array<string> {
        const users = this.db.get("users");
        const followers = new Array<string>();
        Object.values(users).forEach((user) => {
            if (user.followedUsers.includes(nickname)) {
                followers.push(user.nickname);
            }
        });
        return followers;
    }

}