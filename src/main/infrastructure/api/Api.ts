import express, { Request, Response } from "express";
import FollowUserAction from "../../actions/FollowUserAction";
import GetTweetsAction from "../../actions/GetTweetsAction";
import GetUserFollowersAction from "../../actions/GetUserFollowersAction";
import RegisterUserAction from "../../actions/RegisterUserAction";
import TweetAction from "../../actions/TweetAction";
import UpdateUserNameAction from "../../actions/UpdateUserNameAction";
import SimpleDbTweetRepository from "../services/SimpleDbTweetRepository";
import SimpleDbUserRepository from "../services/SimpleDbUserRepository";
import TweetHandler from "./handlers/TweetHandler";
import UserHandler from "./handlers/UserHandler";

const app = express();
app.use(express.json());

const userRepository = new SimpleDbUserRepository();
const tweetRepository = new SimpleDbTweetRepository();

const registerUserAction = new RegisterUserAction(userRepository);
const followUserAction = new FollowUserAction(userRepository);
const getUserFollowersAction = new GetUserFollowersAction(userRepository);
const updateUserNameAction = new UpdateUserNameAction(userRepository);

const tweetAction = new TweetAction(userRepository, tweetRepository);
const getTweetsAction = new GetTweetsAction(userRepository, tweetRepository);

const userHandler = new UserHandler(
    followUserAction,
    getUserFollowersAction,
    registerUserAction,
    updateUserNameAction
);

const tweetHandler = new TweetHandler(
    tweetAction,
    getTweetsAction
)

app.get("/ping", (req: Request, res: Response) => {
    res.send("pong!");
})

app.post("/users", userHandler.register.bind(userHandler));
app.post("/users/follow", userHandler.follow.bind(userHandler));
app.put("/users", userHandler.updateName.bind(userHandler));
app.get("/users/:nickname/followers", userHandler.getFollowers.bind(userHandler));

app.get("/tweets/:nickname", tweetHandler.getTweets.bind(tweetHandler));
app.post("/tweets/add", tweetHandler.addTweet.bind(tweetHandler));

app.listen(3000, () => {
    console.log("listening on port 3000");
});

