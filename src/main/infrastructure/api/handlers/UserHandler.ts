import { Request, Response } from "express";
import UpdateUserNameAction from "../../../actions/UpdateUserNameAction";
import FollowUserAction from "../../../actions/FollowUserAction";
import GetUserFollowersAction from "../../../actions/GetUserFollowersAction";
import RegisterUserAction from "../../../actions/RegisterUserAction";

export default class UserHandler {
    private folowUserAction: FollowUserAction;
    private getUserFollowersAction: GetUserFollowersAction;
    private registerUserAction: RegisterUserAction;
    private updateUserNameAction: UpdateUserNameAction;

    constructor(
        folowUserAction: FollowUserAction,
        getUserFollowersAction: GetUserFollowersAction,
        registerUserAction: RegisterUserAction,
        updateUserNameAction: UpdateUserNameAction) {
        this.folowUserAction = folowUserAction;
        this.getUserFollowersAction = getUserFollowersAction;
        this.registerUserAction = registerUserAction;
        this.updateUserNameAction = updateUserNameAction;
    }


    public register(req: Request, res: Response) {
        if (!req.body) {
            res.status(400);
            res.send("Invalid data");
        }

        if (!req.body.nickname || !req.body.name) {
            res.status(400);
            res.send("User name and nickname are required");
        }

        try {
            const user = this.registerUserAction.execute({
                name: req.body.name,
                nickname: req.body.nickname
            });
            res.status(201);
            res.json(user);
        } catch (error) {
            res.status(400);
            res.send(error.message);
        }

    }

    public follow(req: Request, res: Response) {
        this.validateBody(req, res);
        if (!req.body.nickname || !req.body.nicknameToFollow) {
            res.status(400);
            res.send("User nickname and nickname to follow are required");
        }
        try {
            this.folowUserAction.execute(req.body);
            res.status(200).send("User followed")
        } catch (error) {
            res.status(400).send(error.message);
        }
    }

    public getFollowers(req: Request, res: Response) {
        this.validateBody(req, res);
        if (!req.params.nickname) {
            res.status(400).send("Nickname is required");
        }

        try {
            const followers = this.getUserFollowersAction.execute({ nickname: req.params.nickname });
            res.status(200).json(followers);
        } catch (error) {
            res.status(400).send(error.message);
        }

    }

    public updateName(req: Request, res: Response) {
        this.validateBody(req, res);
        if (!req.body.nickname || !req.body.name) {
            res.status(400).send("Nickname and name are required");
        }
        try {
            const user = this.updateUserNameAction.execute(req.body);
            res.status(200).json(user);
        } catch (error) {
            res.status(400).send(error.message);
        }
    }

    private validateBody(req: Request, res: Response) {
        if (!req.body) {
            res.status(400);
            res.send("Invalid data");
        }
    }
}