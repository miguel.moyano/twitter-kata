import { Request, Response } from "express";
import GetTweetsAction from "../../../actions/GetTweetsAction";
import TweetAction from "../../../actions/TweetAction";

export default class TweetHandler {
    private tweetAction: TweetAction;
    private getTweetsAction: GetTweetsAction;

    constructor(tweetAction: TweetAction, getTweetsAction: GetTweetsAction) {
        this.tweetAction = tweetAction;
        this.getTweetsAction = getTweetsAction;
    }

    public addTweet(req: Request, res: Response) {
        if (!req.body) {
            res.status(400).send("Invalid data");
        }

        if (!req.body.nickname || !req.body.message) {
            res.status(400).send("Nickname and message are required");
        }

        try {
            const tweet = this.tweetAction.execute({
                nickname: req.body.nickname,
                message: req.body.message
            });
            res.status(201).json(tweet);

        } catch (error) {
            res.status(400).send(error.message);
        }
    }

    public getTweets(req: Request, res: Response) {
        if (!req.params) {
            res.status(400).send("Invalid data");
        }

        if (!req.params.nickname) {
            res.status(400).send("Nickname is required");
        }

        try {
            const tweets = this.getTweetsAction.execute({
                nickname: req.params.nickname
            });
            res.status(200).json(tweets);

        } catch (error) {
            res.status(400).send(error.message);
        }
    }
}