import User from "./User";

export default interface UserRepository {
    add(user: User): void;
    getByNickname(nickname: string): User;
    update(user: User): void;
    getFollowersByNickname(nickname: string): Array<string>;
}