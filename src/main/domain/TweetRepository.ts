import Tweet from "./Tweet";

export default interface TweetRepository {
    getTweets(nickname: string): Array<Tweet>;
    add(tweet: Tweet): void;
}