export default class User {
    private name: string;
    private nickname: string;
    private followedUsers: Array<string>;

    constructor(name: string, nickname: string) {
        this.name = name;
        this.nickname = nickname;
        this.followedUsers = new Array();
    }

    public getName() {
        return this.name;
    }

    public getNickname() {
        return this.nickname;
    }

    public setName(name: string) {
        this.name = name;
    }

    public getFollowed(): Array<string> {
        return this.followedUsers;
    }

    public follow(nickname: string) {
        this.followedUsers.push(nickname);
    }

    public setFollowed(followed: Array<string>) {
        this.followedUsers = followed;
    }
}