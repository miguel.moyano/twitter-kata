export default interface Tweet {
    nickname: string;
    message: string
}